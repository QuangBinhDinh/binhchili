﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(6.0f, 4.0f, 6.0f);
        transform.eulerAngles = new Vector3(26.0f, -135.0f, 0.0f);//góc quay theo world (lấy tâm là world space)
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            transform.localEulerAngles = new Vector3(26.0f, -135.0f, 0.0f);//góc quay theo parent (lấy tâm theo parent)

        }else if (Input.GetMouseButtonDown(1))
        {
            transform.eulerAngles = new Vector3(26.0f, -135.0f, 0.0f);
        }
    }
}
