﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMoveConstant : MonoBehaviour
{
    public Transform desNoRigid;
    Rigidbody rb;

     Vector3 constVel;

    public float constspeed;

    private bool canMove;

    private bool moving;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        canMove = true;
        Vector3 direct = desNoRigid.position - transform.position;
        constVel = direct.normalized * constspeed;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!canMove) return;
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                if (hit.collider.name == "CubeNoRigid" && !moving)
                {
                    Debug.Log("moving");
                    rb.AddForce(constVel, ForceMode.VelocityChange);
                    moving = true;
                }
             
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            rb.velocity = Vector3.zero;moving = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.collider.name);
        if (collision.collider.name == "CubeNoRigid" ) canMove = false;
    }
}
