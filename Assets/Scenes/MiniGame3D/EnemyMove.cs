﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyMove : MonoBehaviour
{
    [SerializeField] float m_enemySpeed;

    [SerializeField] TextMeshProUGUI m_text;

    [SerializeField] PlayerMove player;

    [SerializeField] Animator m_enemyAnim;

    Rigidbody rb;

    Renderer render;

    bool m_canMove;

    bool m_directRight;

    float m_timeIdle;

    float m_timeDanger;


    enum EnemyState
    {

        Idle,Shaking,Move,
    }

    private EnemyState m_state;



    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        render = GetComponent<Renderer>();
        render.material.SetColor("_Color", Color.white);
        m_canMove = true;
        m_timeDanger = -1f;
        m_timeIdle = 0f;
        m_directRight = true;


        m_state = EnemyState.Idle;

    }


    void Update()
    {
        //if (player.m_GameOver) return;
        //else if (player.m_GamePause)
        //{
        //    rb.velocity = Vector3.zero;
        //    return;
        //}


        //EnemyChangeStatus();
        Debug.Log(transform.position + " " + rb.velocity);
    }

    //void Start()
    //{
        
    //    StartCoroutine(HandleState());
    //}

    void EnemyChangeStatus()
    {//thay doi trang thai enemy
        if (m_timeDanger == -1f)
        {
          
            m_timeIdle += Time.deltaTime;
            if (m_timeIdle >= 2f)
            { // het 2s chuyen idle->danger

                m_enemyAnim.SetTrigger("EnterDanger");
                
                
                render.material.SetColor("_Color", Color.red);
                m_timeIdle = -1f;
                m_timeDanger = 0f;
                m_canMove = true;
                return;
            }

        }

        if (m_timeIdle == -1f)
        {
            MoveLeftOrRight();//di chuyen enemy
            m_timeDanger += Time.deltaTime;
            if (m_timeDanger >= 2f)
            {
                //het 2s chuyen danger->idle
                render.material.SetColor("_Color", Color.white);
                m_timeDanger = -1f;
                m_timeIdle = 0f;
                m_canMove = false;
                rb.velocity = Vector3.zero;

            }

        }
    }

  

    IEnumerator HandleState()
    {
        while (!player.m_GameOver)
        {
            switch (m_state)
            {
                case EnemyState.Idle:
                    render.material.SetColor("_Color", Color.white);
                    yield return new WaitForSeconds(2f);
                    m_state = EnemyState.Shaking;
                    break;
                case EnemyState.Shaking:
                    m_enemyAnim.SetTrigger("EnterDanger");
                    yield return new WaitForSeconds(0.2f);
                    m_state = EnemyState.Move;
                    break;
                case EnemyState.Move:
                    render.material.SetColor("_Color", Color.red);
                    Vector3 direct = m_directRight ? Vector3.right : Vector3.left;
                    rb.velocity = direct * m_enemySpeed;
                    
                    yield return new WaitUntil(TouchWall);
                    rb.velocity = Vector3.zero;

                    if (m_directRight) m_directRight = false;
                    else m_directRight = true;

                    m_state = EnemyState.Idle;
                    break;

            }
        }
       
    }

    bool TouchWall()
    {
        if (transform.position.x >= 4.3 || transform.position.x <= -4.7) return true;
        else return false;
      
    }

    void MoveLeftOrRight()
    { //di chuyen enemy trai hay phai
        if (transform.position.x <= -4.7) m_directRight = true;
        else if (transform.position.x >= 4.3) m_directRight = false;

        if (m_canMove)
        {
            Vector3 direct = m_directRight ? Vector3.right : Vector3.left;
            rb.velocity = direct * m_enemySpeed;

        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.name == "Player")
        {
            player.m_GameOver = true;
        }
    }
}
