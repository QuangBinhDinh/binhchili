﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandleClcik : MonoBehaviour
{
    [SerializeField] Button m_button;

    [SerializeField] GameObject m_continue;

    [SerializeField] GameObject m_pause;

    [SerializeField] PlayerMove m_player;

    void Start()
    {
        m_button.onClick.AddListener(PauseGame);
    }

    void PauseGame()
    {
        if (!m_player.m_GamePause)
        {
            //pause game
            m_player.m_GamePause = true;
            m_pause.SetActive(false);
            m_continue.SetActive(true);
            
        }
        else
        {
            //continue
            m_player.m_GamePause = false;
            m_continue.SetActive(false);
            m_pause.SetActive(true);
        }
    }
}
