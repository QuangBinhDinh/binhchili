﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMove : MonoBehaviour
{
    Rigidbody rb;

    Renderer render;

    [SerializeField] TextMeshProUGUI m_text;

    [SerializeField] float m_playerSpeed;

    [SerializeField] Animator anim;

    [HideInInspector]
    public bool m_GamePause;

    [HideInInspector]
    public bool m_GameOver;

    [HideInInspector]
    public bool m_GameWin;



    bool m_canMove;

    bool m_freeMove;

    bool m_playerSelected;


    bool m_timeRunning;

    float m_timeRemain;//tinh theo giay

    float m_timeIdle;

    float m_timeActive;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        render = GetComponent<Renderer>();
        render.material.SetColor("_Color", Color.white);

        m_GamePause = false;
        m_GameOver = false;
        m_GameWin = false;

        m_canMove =false;//ban dau player trang thai idle, khong the di chuyen
        m_freeMove = true;//freeMove = true thi chua the di chuyen theo chuot
        m_playerSelected = false;

        m_timeRemain = 100f; 
        m_timeRunning = true;
        m_GameWin = false;
        DisplayTimeLeft(m_timeRemain);

        m_timeIdle = 0f;
        m_timeActive = -1f;

        
    }

    

    // Update is called once per frame
    void Update()
    {

        if ( m_GameOver)
        {
            m_text.text = "Game Over!";
            anim.enabled = false;
            return;
        }else if (m_GamePause)
        {
            rb.velocity = Vector3.zero;
            m_text.text = "Game paused";
            return;
        }else if (m_GameWin)
        {
            m_canMove = false;
            m_text.text = "You win !";
        }
        PlayerChangeStatus();

        if (Input.GetMouseButton(0) && m_canMove)//ghi nho: getMouseButton phai dung trong Update, dung trong FixUpdate thi thoang se bi miss
        {
           
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                if (hit.collider.name == "Player")
                {//case user nhan chuot vao player, player di chuyen theo con chuot
                    m_playerSelected = true;
                    rb.velocity = Vector3.zero;
                    m_freeMove = false;
                    transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);


                }
                else if (hit.collider.name == "Plane" && !m_freeMove && m_playerSelected)
                {
                    //truong hop chuot di chuyen qua nhanh
                    transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                }
                else m_playerSelected = false;



            }

            if (m_freeMove)
            {
                rb.velocity = transform.forward * m_playerSpeed;


            }
        }
        else if (Input.GetMouseButtonUp(0) && m_canMove)
        {
            if (m_freeMove)
            {
                rb.velocity = Vector3.zero;
                Debug.Log("stopped");
            }
            else m_playerSelected = false;
           
           
        }

        if (m_timeRunning && m_timeRemain > 0f)
        {
            m_timeRemain -= Time.deltaTime;
            DisplayTimeLeft(m_timeRemain);
        }
        else if (m_timeRemain <= 0f)
        {
            m_text.text = "Time out!";
        }

        
    }

    void OnCollisionEnter(Collision collision)//xu ly va cham
    {
        if(collision.collider.name == "FinishLine")
        {
            m_timeRunning = false;
            Debug.Log("collide");
            rb.velocity = Vector3.zero;
            m_GameWin = true;
           
            
        }
    }

    void DisplayTimeLeft(float timeLeft)
    {
        //hien thi countdown
        timeLeft += 1;
        float minutes = Mathf.FloorToInt(timeLeft / 60);
        float seconds = Mathf.FloorToInt(timeLeft % 60);
        m_text.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void PlayerChangeStatus()
    {//thay doi trang thai player
        if (m_timeActive == -1f)
        {
            m_timeIdle += Time.deltaTime;
            if(m_timeIdle >= 1.5f)
            { // het 1.5s chuyen idle->active
                anim.enabled = false;
                render.material.SetColor("_Color", Color.green);
                m_timeIdle = -1f;
                m_timeActive = 0f;
                m_canMove = true;
                rb.velocity = Vector3.zero;
                
                return;
            }
           
        }

        if(m_timeIdle == -1f)
        {
            m_timeActive += Time.deltaTime;
            if(m_timeActive >= 0.5f)
            {
                //het0.5s chuyen active->idle
                anim.enabled = true;
                render.material.SetColor("_Color", Color.white);//chuyen sang mau trang khi player idle
                m_timeActive = -1f;
                m_timeIdle = 0f;
                m_canMove = false;
                rb.velocity = Vector3.zero;
                
            }
            
        }
    }

   
}
