﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate1Script : MonoBehaviour
{
    // Start is called before the first frame update
    
    [SerializeField] Transform cubeTrans;
    [SerializeField] Camera cam;

    Ray ray;
    RaycastHit hit;

    string m_tagPlane = "Plane";

    Vector3 m_rotateAxis;
   

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            
            if(Physics.Raycast(ray,out hit, Mathf.Infinity))
            {
                
                if (hit.transform.CompareTag(m_tagPlane) )
                {
                    m_rotateAxis = hit.point - transform.position;

                    Debug.DrawRay(transform.position, m_rotateAxis, Color.green);
                    cubeTrans.RotateAround(hit.point, m_rotateAxis, 180 * Time.deltaTime);
                }
            }
        }
    }
}
