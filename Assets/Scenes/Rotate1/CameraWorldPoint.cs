﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWorldPoint : MonoBehaviour
{
    Camera cam;

     void Awake()
    {
        cam = Camera.main;
    }


    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.farClipPlane);

            Vector3 mousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.nearClipPlane);

            Vector3 mouseWorldFar = cam.ScreenToWorldPoint(mousePosFar);

            Vector3 mouseWorldNear = cam.ScreenToWorldPoint(mousePosNear);

            Vector3 direction = mouseWorldFar - mouseWorldNear;

            Debug.DrawRay(mouseWorldNear, direction, Color.green);

            Debug.Log(mousePosNear + " " + mouseWorldNear);

        }
    }
}
