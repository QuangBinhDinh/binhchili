﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate2 : MonoBehaviour
{

    Vector3 m_posFarBefore;
    Vector3 m_posNearBefore;

    Vector3 m_initialDirect;

    Vector3 m_afterDirect;

    float m_signAngle;

    float m_currentAngle;

    Vector3 m_rotate;

    [SerializeField] Camera cam;

   

    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {
            m_initialDirect = CalculateDirection();//tinh vector start
        }
        else if (Input.GetMouseButton(0))
        {

         
            m_afterDirect = CalculateDirection();//tinh vector destination

            m_signAngle = Vector3.SignedAngle(Vector3.ProjectOnPlane(m_afterDirect,Vector3.up), Vector3.ProjectOnPlane(m_initialDirect,Vector3.up), Vector3.up);//tinh goc quay khi di chuyen chuot (tung frame)

            //projectOnPlane de chieu 1 vector xuong 1 mat phang vuong goc voi vector duoc truyen vao ham
            //do ta can tinh goc quay chieu ngang (mat phang Oxz) nen se chieu xuong mat phang nay
            
            m_currentAngle = WrapAngle(transform.localEulerAngles.y);//luu y dung localEulerAngle de lay gia tri quay cua gameObj

            if((m_currentAngle <= 45 && m_signAngle >0) || (m_currentAngle >= -45 && m_signAngle < 0)) {
                //kiem tra xem goc quay cua camera dat gioi han chua
                m_rotate = new Vector3(0, m_signAngle, 0);


                transform.Rotate(m_rotate);//quay 1 goc tuong ung voi khoang cach di chuyen cua chuot

            }

            m_initialDirect = CalculateDirection();// dat lai vector start

            Debug.Log(m_signAngle);

        }

       

    }

    Vector3 CalculateDirection()//tinh vector direction tu camera den vi tri con tro chuot trong khong gian 3d
    {
        m_posFarBefore = new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.farClipPlane);

        

        Vector3 mouseWorldFar = cam.ScreenToWorldPoint(m_posFarBefore);

        Debug.DrawRay(transform.position, mouseWorldFar - transform.position, Color.green);
        return mouseWorldFar - transform.position;

        

        

    }

     float WrapAngle(float angle)
    {//do vector Quaternion có range (0,360) khác với trong inspector (-180,180) nên dùng hàm này để chuyển từ Quaternion -> inspector
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }

     float UnwrapAngle(float angle)
    {//chuyển ngược lại
        if (angle >= 0)
            return angle;

        angle = -angle % 360;

        return 360 - angle;
    }
}
