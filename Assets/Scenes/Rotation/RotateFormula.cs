﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFormula : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform sphereTrans;

    void Start()
    {
        sphereTrans.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Space.self rotate objec theo truc toa do chinh object
        //con Space.World rotate obj theo truc global
        transform.Rotate(Vector3.up, 180 * Time.deltaTime, Space.World);
        transform.Translate(transform.forward * 1 * Time.deltaTime);
        
        
    }
}
