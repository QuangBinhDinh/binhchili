﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CamScript : MonoBehaviour
{
    
    
    public GameObject cube;
    public Camera cam;

    Ray m_ray;

    RaycastHit[] hitList;

    RaycastHit hit;

    string cubeName = "Player";
    string planeName = "Plane";

   

    bool m_selected;
   
    void Awake()
    {
        m_selected = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
             m_ray = cam.ScreenPointToRay(Input.mousePosition);

             hitList = Physics.RaycastAll(m_ray, Mathf.Infinity);

            if (hitList.Any(i=>i.transform.CompareTag(cubeName))) m_selected = true;

            if(hitList.Any(i => i.transform.CompareTag(planeName)) && m_selected)
            {
                hit = hitList.Where(i => i.collider.name == planeName).ToList()[0];
                cube.transform.position = new Vector3(hit.point.x, 0.5f, hit.point.z);
               
            }
           
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_selected = false;
        }
    }
}
