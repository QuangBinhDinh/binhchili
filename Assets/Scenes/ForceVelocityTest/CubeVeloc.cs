﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeVeloc : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;

    
    public float turnPower;
         
    void Start()
    {
        
    }


    void Update()
    {
        float turn = Input.GetAxis("Horizontal");
        float accel = Input.GetAxis("Vertical");

        Quaternion turnAngle = Quaternion.AngleAxis(turn * turnPower, transform.up);
        Vector3 forward = turnAngle * transform.forward;
        //ghi nhớ: khi dùng forward này thì hướng sẽ rotate nhưng object thì không
        //muốn cả object cũng rotate theo thì phải dùng transform.Rotate

        Vector3 adjustVelocity = rb.velocity + accel * speed * forward * Time.deltaTime;


        rb.velocity = adjustVelocity;

    }

    //void Update()
    //{

    //    float turn = Input.GetAxis("Horizontal");
    //    float accel = Input.GetAxis("Vertical");

    //    transform.Rotate(transform.up, turn * turnPower);
    //    Vector3 adjustVec = rb.velocity + accel * speed * transform.forward * Time.deltaTime;

    //    rb.velocity = adjustVec;
    //}
}
