﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeText : MonoBehaviour
{
    //script gan vao obj Level cua prefab
    TextMeshProUGUI textMesh;

    void Awake()
    {
        textMesh = GetComponent<TextMeshProUGUI>();

        //lay textmeshpro ra
    }

  

    public void changeInput(string s)
    {
        textMesh.text = s;
    }
}
