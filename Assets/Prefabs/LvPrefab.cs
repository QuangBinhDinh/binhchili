﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LvPrefab : MonoBehaviour
{
    //script gan vao prefab level 
    // Script nay se modify 4 gameobject duoi day

    
    [SerializeField] TextMeshProUGUI textMesh;
    [SerializeField] GameObject[] m_Stars;

    public void ModifyLevel(int num, int random)
    {//num la value cua textInput, con random la so sao
       
        textMesh.text = num.ToString();
        
        for (int i = 0; i < m_Stars.Length; i++)
        {
            m_Stars[i].SetActive(i < random);
        }
    }
}
